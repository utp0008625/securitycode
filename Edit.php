<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Edit extends CI_Controller
{

	//La funcion contructor carga antes que todas las demas funciones... en este caso antes que el index.
	public function __construct()
	{
		parent::__construct();
		//aqui cargaremos nuestro modelo estudiante
		$this->load->model("estudiante_model");
	}

	public function index($id)
	{
        // echo  $id;
        $datos = $this->estudiante_model->obtenerEstudiante($id);
		$this->load->view('estudiantes/edit',$datos);
        // print_r($datos);
	}

	public function actualizar($id)
	{
		$nombre_completo = $this->input->post("nombre_completo");
		$ape_paterno = $this->input->post("ape_paterno");
		$ape_materno = $this->input->post("ape_materno");
		$correo = $this->input->post("correo");
		


        //recuperamos nuestro correo desde la base de datos
        $datos = $this->estudiante_model->obtenerEstudiante($id);

        $validacionCorreo="";

        if($correo!=$datos->correo){
            $validacionCorreo="|is_unique[estudiantes.correo]";
        }
		//validaciones
		$this->form_validation->set_rules('nombre_completo', '"Nombre completo"', 'required|min_length[3]');
		$this->form_validation->set_rules('correo', '"Correo electronico"', 'required|valid_email'.$validacionCorreo);
	
		//en caso de que los datos sean incorrectos
		if ($this->form_validation->run() == FALSE) {
            // $datos = $this->estudiante_model->obtenerEstudiante($id);
			// $this->load->view("estudiantes/edit",$datos);
            //aqui cargamos nuestro index nuevamente
            $this->index($id);
		} else {
			//si son correctos se guardan los datos en un array
			$data = array(
				"nombre" => $nombre_completo,
				"apellido_paterno" => $ape_paterno,
				"apellido_materno" => $ape_materno,
				"correo" => $correo
			);

			$this->estudiante_model->actualizar($data,$id);
			//el mensaje que se registro correctamente
			$this->session->set_flashdata('success', 'Los datos se actualizaron correctamente');
			redirect(base_url()."estudiantes");
		}
	}
}
